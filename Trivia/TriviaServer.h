#pragma comment (lib, "ws2_32.lib")
#pragma once

#include <mutex>
#include <iostream>
#include <map>
#include <queue>
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include "Validator.h"
#include <condition_variable>
#include <exception>

#include "User.h"
#include "sqlite3.h"
#include "DataBase.h"
#include "Room.h"
#include "RecievedMessage.h"
#include "WSAInitializer.h"
#include "Helper.h"
#include "MessageCodes.h"
#include "md5.h"


#define PORT 8820
#define END 299


#define USERNAME_INDEX 5
#define MESSAGE_CODE_SIZE 3
#define SIZE_SIZE 2
#define ID_SIZE 4
#define PLAYERS_NUM_SIZE 1
#define QUESTION_NUM_SIZE 2
#define TTA_SIZE 2
#define ANSWER_NUMBER_SIZE 1
#define ANSWER_TIME_SIZE 2

#define ROOM_NOT_CLOSED -1
using namespace std;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();


private:
	void bindAndListen();
	void clientHandler(SOCKET socket);
	void accept();
	void safeDeleteUser(RecievedMessage* msg);

	void handleRecievedMessages();
	RecievedMessage* getMessage();
	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);
	bool handleCreateRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);
	bool handlejoinRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buildRecievedMessage(SOCKET socket, int msgCode);

	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);
	Room* getRoomById(int roomId);

	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomsList;
	mutex _mtxReciveMessages;
	queue<RecievedMessage*> _queRcvMessages;
	int _roomIdSequence;
	condition_variable _cv;

};

