#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;
	_answers[0] = correctAnswer;
	_answers[1] = answer2;
	_answers[2] = answer3;
	_answers[3] = answer4;
	_answers[4] = "";
	
	shuffle(_answers, NUMBER_OF_ANSWERS - 1);
	
	for (int i = 0; i < NUMBER_OF_ANSWERS; i++)
	{
		if (_answers[i].compare(correctAnswer) == 0) // save the index of the correct answer
		{
			_correctAnswerIndex = i;
			break;
		}
	}
}

string Question::getQuestion()
{
	return _question;
}

string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}

void Question::shuffle(string arr[], int size)
{
	int j = 0;
	for (int i = 0; i < size; i++)
	{
		j = rand() % size;
		swap(&arr[i], &arr[j]);
	}
}

void Question::swap(string *a, string *b)
{
	string temp = *a;
	*a = *b;
	*b = temp;
}
