#pragma once
#include <vector>
#include <string>
#include "Helper.h"
#include "MessageCodes.h"

class User;

#define ROOM_NOT_CLOSED -1
#define QUESTION_LENGTH 2


class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();

private:
	void sendMessage(User* user, string message);
	void sendMessage(string message);

	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionsTime;
	int _questionNo;
	string _name;
	int _id;
};

