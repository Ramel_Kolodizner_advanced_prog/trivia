#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool ret = true;
	if (password.length() < PASSWORD_MIN_LENGTH)
	{
		ret = false;
	}
	else if (password.find(' ') != string::npos)
	{
		ret = false;
	}
	else if (password.find_first_of(DIGITS) == string::npos)
	{
		ret = false;
	}
	else if (password.find_first_of(UPPER_CASES) == string::npos)
	{
		ret = false;
	}
	else if (password.find_first_of(LOWER_CASES) == string::npos)
	{
		ret = false;
	}
	return ret;
}

bool Validator::isUsernameValid(string username)
{
	bool ret = true;
	if (!(isalpha(username[0])))
	{
		ret = false;
	}
	else if (username.find(' ') != string::npos)
	{
		ret = false;
	}
	else if (username.length() == 0)
	{
		ret = false;
	}
	return ret;
}
