#pragma once

#include <string>
#include <algorithm>
#include <time.h>

using namespace std;

#define NUMBER_OF_ANSWERS 5 

class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();
private:
	void shuffle(string arr[], int size);
	void swap(string* a, string* b);
	string _question;
	string _answers[NUMBER_OF_ANSWERS];
	int _correctAnswerIndex;
	int _id;
};

