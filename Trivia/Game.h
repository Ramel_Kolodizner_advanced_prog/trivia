#pragma once
#include <vector>
#include "Question.h"
#include "DataBase.h"
#include <map>
#include <string>
#include <iterator>
#include "Helper.h"
#include "MessageCodes.h"

class User;
using namespace std;

#define QUESTION_SIZE_LENGTH 3

class Game
{
public:
	Game(const vector<User*>&, int, DataBase&);
	~Game();

	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerIndex, int answerTime);
	bool leaveGame(User* user);
	int getID();



private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _id;

	void initResults();
	void setGameForAllUsers();
	void sendQuestionToAllUsers();
	
};

