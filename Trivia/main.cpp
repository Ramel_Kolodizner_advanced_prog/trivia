#pragma comment (lib, "ws2_32.lib")
#include "DataBase.h"
#include "Question.h"
#include "TriviaServer.h"

#include <iostream>

using namespace std;

int main()
{
	srand(time(NULL));
	
	try
	{
		WSAInitializer i;
		TriviaServer server;
		server.serve();
	}
	catch (const std::exception& e )
	{
		cout << e.what() << endl;
	}
}