#pragma once
#include <string>

#define PASSWORD_MIN_LENGTH 4
#define DIGITS "0123456789"
#define UPPER_CASES "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define LOWER_CASES "abcdefghijklmnopqrstuvwxyz"

using namespace std;

class Validator
{
public:	
	static bool isPasswordValid(string password);
	static bool isUsernameValid(string username);
};

