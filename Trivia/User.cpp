#include "User.h"

User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
}

void User::send(string message)
{
	Helper::sendData(_sock, message);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room * User::getRoom()
{
	return _currRoom;
}

Game * User::getGame()
{
	return _currGame;
}

void User::setGame(Game * gm)
{
	_currGame = gm;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomID, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr) // user Belongs to a room
	{
		send(to_string(MC_CREATE_ROOM_RESPONSE_FAIL)); // fail
		return false;
	}
	else
	{
		_currRoom = new Room(roomID, this, roomName, maxUsers, questionsNo, questionTime);
		if (_currRoom == nullptr)
		{
			send(to_string(MC_CREATE_ROOM_RESPONSE_FAIL)); // fail
			return false;
		}
		else
		{
			send(to_string(MC_CREATE_ROOM_RESPONSE_SUCCESS)); // success
			return true;
		}
	}
}

bool User::joinRoom(Room * newRoom)
{
	if (_currRoom != nullptr) // user Belongs to a room
		return false;
	else
	{
		newRoom->joinRoom(this);
		_currRoom = newRoom;
		return true;
	}
}

void User::leaveRoom()
{
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (_currRoom == nullptr)
		return -1;
	else
	{
		int val = _currRoom->closeRoom(this);
		if (val != -1) // room closed successfully
			_currRoom = nullptr;
		return val;
	}
}

bool User::leaveGame()
{
	if (_currGame != nullptr) // belongs to a game
	{
		bool val = _currGame->leaveGame(this); 
		_currGame = nullptr;
		return val;
	}
	return false;
}
