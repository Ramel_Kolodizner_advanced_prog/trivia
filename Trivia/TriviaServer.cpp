#include "TriviaServer.h"



TriviaServer::TriviaServer()
{ 
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - socket");
	//TODO- check if static
	_roomIdSequence = 0;
}

TriviaServer::~TriviaServer()
{
	delete (&_roomsList);
	delete (&_connectedUsers);
	// close Socket
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}


void TriviaServer::serve()
{
	thread t(&TriviaServer::handleRecievedMessages, this);
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration 
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << "Listening on port " << PORT << endl;
}

void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	thread t(&TriviaServer::clientHandler, this, client_socket);
	t.detach();
}

User * TriviaServer::handleSignin(RecievedMessage * msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];

	crypto::md5_helper_t md5;
	bool isUserAndPassMatch = _db.isUserAndPassMatch(username, md5.hexdigesttext(password));
	if (!isUserAndPassMatch) // user dosen't exist
	{
		Helper::sendData(msg->getSock(), to_string(MC_SIGN_IN_RESPONSE_WRONG_DETAILS));
		return nullptr;
	}
	else // user exists
	{
		bool connected = getUserByName(username);
		if (connected)
		{
			Helper::sendData(msg->getSock(), to_string(MC_SIGN_IN_RESPONSE_USER_IS_ALREADY_CONNECTED));
			return nullptr;
		}
		else
		{
			User* u = new User(username, msg->getSock());
			_connectedUsers.insert(pair<SOCKET, User*>(msg->getSock(), u));
			Helper::sendData(msg->getSock(), to_string(MC_SIGN_IN_RESPONSE_SUCCESS));
			return u;
		}
	}
}

bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	bool validPassword = Validator::isPasswordValid(password); // Check whether password is valid
	if (!validPassword)
	{
		Helper::sendData(msg->getSock(), to_string(MC_SIGN_UP_RESPONSE_PASS_ILLEGAL));
		return false;
	}

	bool validUsername = Validator::isPasswordValid(password);  // Check whether username is valid
	if (!validPassword)
	{
		Helper::sendData(msg->getSock(), to_string(MC_SIGN_UP_RESPONSE_USERNAME_IS_ILLEGAL));
		return false;
	}

	bool isExist = _db.isUserExist(username);
	if (isExist) // username is already exists
	{
		Helper::sendData(msg->getSock(), to_string(MC_SIGN_UP_RESPONSE_USERNAME_IS_ALREADY_EXISTS));
		return false;
	}
	else
	{
		crypto::md5_helper_t md5;
		bool sucssesfulInsertion = _db.addNewUser(username, md5.hexdigesttext(password), email);
		if (sucssesfulInsertion)
		{
			Helper::sendData(msg->getSock(), to_string(MC_SIGN_UP_RESPONSE_SUCCESS));
			return true;
		}
		else
		{
			Helper::sendData(msg->getSock(), to_string(MC_SIGN_UP_RESPONSE_OTHER));
			return true;
		}
	}

}

void TriviaServer::handleSignout(RecievedMessage * msg)
{
	if (msg->getUser() != nullptr)
	{
		SOCKET s = msg->getSock();
		/* Delete user from map */
		map<SOCKET, User*>::iterator itr = _connectedUsers.begin();
		while (itr != _connectedUsers.end()) 
		{
			if (itr->first == s)
			{
				itr = _connectedUsers.erase(itr);
			}
			else
				++itr;
		}
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);

		delete msg->getUser();
	}
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	Room* r = msg->getUser()->getRoom();
	if (!r) // no room	
		return false;
	else
	{
		int close = msg->getUser()->closeRoom();
		if (close != -1) // success
		{
			map<int, Room*>::iterator itr = _roomsList.begin();
			while (itr != _roomsList.end())
			{
				if (itr->second == r)
					itr = _roomsList.erase(itr);
				else
					++itr;
			}
			return true;
		}
		else
			return true;
	}

}

bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	User* u = msg->getUser();
	if (!u) // user doesn't exist	
		return false;
	else
	{
		Room* r = u->getRoom();
		if (!r) // room doesn't exist	
			return false;
		else
		{
			u->leaveRoom();
			return true;
		}
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage * msg)
{
	bool leaveGame = msg->getUser()->leaveGame();
	if (leaveGame) // game is over
	{
		msg->getUser()->getGame()->~Game();
	}
}



void TriviaServer::addRecievedMessage(RecievedMessage * message)
{
	lock_guard<mutex> lock(_mtxReciveMessages);
	_queRcvMessages.push(message);
	_cv.notify_one();
}

RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET socket, int msgCode)
{
	vector<string> values;
	string s;
	int size;
	switch (msgCode)
	{
	case MC_SIGN_IN:
	case MC_SIGN_UP:
		s = Helper::getStringPartFromSocket(socket, SIZE_SIZE);
		size = atoi(s.substr(0, SIZE_SIZE).c_str()); // size Of username
		s = Helper::getStringPartFromSocket(socket, size); // username
		values.push_back(s);

		s = Helper::getStringPartFromSocket(socket, SIZE_SIZE);
		size = atoi(s.substr(0, SIZE_SIZE).c_str()); // size Of password
		s = Helper::getStringPartFromSocket(socket, size); // password
		values.push_back(s);

		if (msgCode == MC_SIGN_UP)
		{
			s = Helper::getStringPartFromSocket(socket, SIZE_SIZE);
			size = atoi(s.substr(0, SIZE_SIZE).c_str()); // size Of email
			s = Helper::getStringPartFromSocket(socket, size); // email
			values.push_back(s);
		}
		break;
	case MC_USERS_IN_ROOM:
	case MC_JOIN_ROOM:
		s = Helper::getStringPartFromSocket(socket, ID_SIZE);
		values.push_back(s);
		break;
	case MC_CREATE_ROOM:
		s = Helper::getStringPartFromSocket(socket, SIZE_SIZE);
		size = atoi(s.substr(0, SIZE_SIZE).c_str()); // size Of room name
		s = Helper::getStringPartFromSocket(socket, size); // room name
		values.push_back(s);
		s = Helper::getStringPartFromSocket(socket, PLAYERS_NUM_SIZE); // players number
		values.push_back(s);
		s = Helper::getStringPartFromSocket(socket, QUESTION_NUM_SIZE); // questions number
		values.push_back(s);
		s = Helper::getStringPartFromSocket(socket, TTA_SIZE); // time to answer
		values.push_back(s);
		break;
	case MC_CLIENT_SENT_ANSWER:
		s = Helper::getStringPartFromSocket(socket, ANSWER_NUMBER_SIZE); // answer number
		values.push_back(s);
		s = Helper::getStringPartFromSocket(socket, ANSWER_TIME_SIZE); // answer time
		values.push_back(s);
	default:
		break;
	}
	RecievedMessage* msg = new RecievedMessage(socket, msgCode, values);
	return msg;
}

void TriviaServer::clientHandler(SOCKET socket)
{
	try
	{
		int messageCode = 1;
		while (messageCode != 0 && messageCode != END)
		{
			messageCode = Helper::getMessageTypeCode(socket);
			addRecievedMessage(buildRecievedMessage(socket, messageCode));
		}
		closesocket(socket);
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		addRecievedMessage(buildRecievedMessage(socket, END));
	}

}

User * TriviaServer::getUserByName(string username)
{
	User* val = nullptr;
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->second->getUsername() == username)
		{
			val = it->second;
			break;
		}
	}
	return val;
}

User * TriviaServer::getUserBySocket(SOCKET client_socket)
{
	User* val = nullptr;
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->first == client_socket)
		{
			val = it->second;
			break;
		}
	}
	return val;
}

Room * TriviaServer::getRoomById(int roomId)
{
	Room* val = nullptr;
	map<int, Room*>::iterator it;
	for (it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		if (it->first == roomId)
		{
			val = it->second;
			break;
		}
	}
	return val;
}

void TriviaServer::handleRecievedMessages()
{
	try
	{
		RecievedMessage* message = getMessage();

		while (true)
		{
			message->setUser(getUserBySocket(message->getSock()));

			switch (message->getMessageCode())
			{
			case MC_SIGN_IN:
				handleSignin(message);
				break;
			case MC_SIGN_OUT:
				handleSignout(message);
				break;
			case MC_SIGN_UP:
				handleSignup(message);
				break;
			case MC_ROOMS_LIST:
				handleGetRooms(message);
				break;
			case MC_USERS_IN_ROOM:
				handleGetUsersInRoom(message);
				break;
			case MC_JOIN_ROOM:
				handlejoinRoom(message);
				break;
			case MC_LEAVE_ROOM:
				handleLeaveRoom(message);
				break;
			case MC_CREATE_ROOM:
				handleCreateRoom(message);
				break;
			case MC_CLOSE_ROOM:
				handleCloseRoom(message);
				break;
			case MC_START_GAME:
				handleStartGame(message);
				break;
			case MC_CLIENT_SENT_ANSWER:
				handlePlayerAnswer(message);
				break;
			case MC_LEAVE_GAME:
				handleLeaveGame(message);
				break;
			case MC_BEST_SCORES:
				handleGetBestScores(message);
				break;
			case MC_PERSONAL_STATUS:
				handleGetPersonalStatus(message);
				break;
			default:
				safeDeleteUser(message);
				break;
			}
			message = getMessage();
		}
	}
	catch (const std::exception& e)
	{
		e.what();
	}
}

RecievedMessage* TriviaServer::getMessage()
{
	unique_lock<mutex> lock(_mtxReciveMessages);
	if (_queRcvMessages.empty())
	{
		_cv.wait(lock);
	}

	RecievedMessage* message = _queRcvMessages.front();
	_queRcvMessages.pop();
	lock.unlock();

	return message;
}

void TriviaServer::safeDeleteUser(RecievedMessage * msg)
{
	try
	{
		SOCKET soc = msg->getSock();
		handleSignout(msg);
		::closesocket(soc);
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}
}

bool TriviaServer::handlejoinRoom(RecievedMessage * msg)
{
	if (msg->getUser() != NULL)
	{
		Room* room = getRoomById(stoi(msg->getValues()[0]));
		if (room != NULL)
		{
			msg->getUser()->joinRoom(room);
			return true;
		}
		msg->getUser()->send(to_string(MC_JOIN_ROOM_RESPONSE_ROOM_NOT_EXIST_OR_OTHER_REASON));
	}

	return false;
}


void TriviaServer::handleStartGame(RecievedMessage * msg)
{
	try
	{
		Game* g = new Game(msg->getUser()->getRoom()->getUsers(), msg->getUser()->getRoom()->getQuestionsNo(), _db);
		map<int, Room*>::iterator it;
		for (it = _roomsList.begin(); it != _roomsList.end(); it++)
		{
			if (it->second == msg->getUser()->getRoom())
			{
				break;
			}
		}
		_roomsList.erase(it);
		g->sendFirstQuestion();
	}
	catch (const std::exception& e)
	{
		Helper::sendData(msg->getSock(),to_string(MC_CREATE_ROOM_RESPONSE_FAIL));
		throw(e);
	}
}
void TriviaServer::handlePlayerAnswer(RecievedMessage * msg)
{
	if (msg->getUser()->getGame() != NULL)
	{
		if (!(msg->getUser()->getGame()->handleAnswerFromUser(msg->getUser(), stoi(msg->getValues()[0]), stoi(msg->getValues()[1]))))
		{
			delete msg->getUser()->getGame();
		}
	}
}
bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	if (msg->getUser() != NULL)
	{
		_roomIdSequence++;
		bool val = ((msg->getUser())->createRoom(_roomIdSequence, msg->getValues()[0], stoi(msg->getValues()[1]), stoi(msg->getValues()[2]), stoi(msg->getValues()[3])));
		if (val)
		{
			_roomsList.insert(pair<int, Room*>(_roomIdSequence, msg->getUser()->getRoom()));
			return true;
		}
		else
			return false;
	}
	else
		return false;
}
void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	string messageToUser = to_string(MC_ROOMS_LIST_RESPONSE);
	string room;
	int numOfRooms = _roomsList.size();
	messageToUser += Helper::getPaddedNumber(numOfRooms, 4);
	if (numOfRooms == 0)
		Helper::sendData(msg->getSock(), messageToUser);
	else
	{
		map<int, Room*>::iterator it;
		for (it = _roomsList.begin(); it != _roomsList.end(); it++)
		{
			messageToUser += Helper::getPaddedNumber(it->first, 4);
			messageToUser += Helper::getPaddedNumber((it->second->getName().length()), 2);
			messageToUser += it->second->getName();
		}
	}
	msg->getUser()->send(messageToUser);
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage * msg)
{
	Room* r;
	if ((r = getRoomById(stoi(msg->getValues()[0]))) != NULL)
	{
		msg->getUser()->send(r->getUsersListMessage());
	}
}

void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
	string messageToClient = to_string(MC_BEST_SCORES_RESPONSE);
	DataBase d;
	vector<string> v = d.getBestScores();
	for (int i = 0; i < 9; i++)
	{
		if (i == 0 || i == 3 || i == 6) // username length
			messageToClient += Helper::getPaddedNumber(stoi(v[i]), 2);
		else if (i == 1 || i == 4 || i == 7) // username
			messageToClient += v[i];
		else if (i == 2 || i == 5 || i == 8) // pass
			messageToClient += Helper::getPaddedNumber(stoi(v[i]), 6);
	}
	Helper::sendData(msg->getSock(), messageToClient);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * msg)
{
	string messageToClient = to_string(MC_PERSONAL_STATUS_RESPONSE);
	vector<string> v = _db.getPersonalStatus(msg->getUser()->getUsername());
	for (int i = 0; i < 4; i++)
	{
			if (i == 0) // number of games
				messageToClient += Helper::getPaddedNumber(stoi(v[i]), 4);
			else if (i == 1 || i == 2) // correct or wrong answers
				messageToClient += Helper::getPaddedNumber(stoi(v[i]), 6);
			else if (i == 3) // time
				messageToClient += Helper::getPaddedNumber(stoi(v[i]), 4);
	}
	Helper::sendData(msg->getSock(), messageToClient);
}

