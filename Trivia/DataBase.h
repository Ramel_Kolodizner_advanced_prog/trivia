#pragma once

#include "sqlite3.h"
#include "Question.h"
#include "Helper.h"
#include <exception>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define DATABASE "trivia.db"
#define ANSWERS_LENGTH 6
#define OTHERS_LENGTH 4


class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExist(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*>  initQuestions(int questionsNo);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string username);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	
private:
	sqlite3* db;
	char* zErrMsg;
	char* s;
	static int callbackCount(void* val, int argc, char** argv, char** azCol);
	static int callbackQuestions(void* vec, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* vec, int argc, char** argv, char** azCol);
	static int callbackPersonalStatus(void* counter, int argc, char** argv, char** azCol);
	static int callbackPersonalStatusAvg(void * avg, int argc, char ** argv, char ** azCol);
	static int callbackReturnId(void* notUsed, int argc, char** argv, char** azCol);
};

