#include "Game.h"
#include "User.h"


Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	_id = _db.insertNewGame();
	if (_id == -1)
	{
		throw exception(__FUNCTION__"- insertNewGame");
	}
	_questions = _db.initQuestions(questionsNo);
	_players = players;
	_currentTurnAnswers = 0;
	_currQuestionIndex = 0;
	_questions_no = questionsNo;
	initResults();
	setGameForAllUsers();


}


Game::~Game()
{
	for each (Question* q in _questions)
	{
		//TODO - check how to use distructor
		delete(q);
	}
	_questions.clear();
	_players.clear();
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	_db.updateGameStatus(1);
	string message = to_string(MC_CLOSE_GAME);
	message += to_string(_players.size());
	for each (User* u in _players)
	{
		message += Helper::getPaddedNumber(u->getUsername().length(), 2);
		message += u->getUsername();
		message += Helper::getPaddedNumber(_results[u->getUsername()], 2);
	}
	for each (User* u in _players)
	{
		try
		{
			u->send(message);
		}
		catch (const std::exception& e)
		{
			cout << e.what() << endl;
		}
	}
}

bool Game::handleNextTurn()
{
	bool ret = true;;
	if (!_players.empty())
	{
		if (_currentTurnAnswers == _players.size())
		{
			if (_currQuestionIndex < _questions_no - 1 )
			{
				_currQuestionIndex++;
				sendQuestionToAllUsers();
			}
			else
			{
				handleFinishGame();
				ret = false;
			}
		}
	}
	else
	{
		handleFinishGame();
		ret = false;
	}
	return ret;
}

bool Game::handleAnswerFromUser(User * user, int answerIndex, int answerTime)
{
	_currentTurnAnswers++;
	bool isCorect = false;
	if (answerIndex == _questions[_currQuestionIndex]->getCorrectAnswerIndex() + 1)
	{
		_results[user->getUsername()]++;
		isCorect = true;
	}
	_db.addAnswerToPlayer(getID(), user->getUsername(), _questions[_currQuestionIndex]->getId(), _questions[_currQuestionIndex]->getAnswers()[ _questions[_currQuestionIndex]->getCorrectAnswerIndex()], isCorect, answerTime);
	if (isCorect)
	{
		user->send(to_string(MC_LAST_ANSWER_CORRECT));
	}
	else
	{
		user->send(to_string(MC_LAST_ANSWER_WRONG));
	}
	return handleNextTurn();
}

bool Game::leaveGame(User * user)
{
	for (vector<User*>::iterator i = _players.begin(); i != _players.end(); i++)
	{
		if ((*i)->getUsername() == user->getUsername())
		{
			_players.erase(i);
		}
	}
	return handleNextTurn();
}

int Game::getID()
{
	return _id;
}

void Game::initResults()
{
	vector<User*>::iterator iv = _players.begin();

	while (iv != _players.end())
	{
		_results.insert({ (*iv)->getUsername(), 0 });
		iv++;
	}
}

void Game::setGameForAllUsers()
{
	vector<User*>::iterator iv = _players.begin();

	while (iv != _players.end())
	{
		(*iv)->setGame(this);
		iv++;
	}
}

void Game::sendQuestionToAllUsers()
{
	string message = "118";
	message += Helper::getPaddedNumber( _questions[_currQuestionIndex]->getQuestion().length(), QUESTION_SIZE_LENGTH);
	message += _questions[_currQuestionIndex]->getQuestion();
	for (int i = 0; i < NUMBER_OF_ANSWERS - 1; i++)
	{
		message += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getAnswers()[i].length(), QUESTION_SIZE_LENGTH);
		message += _questions[_currQuestionIndex]->getAnswers()[i];
	}	
	_currentTurnAnswers = 0;

	for each (User* u in _players)
	{
		try
		{
			u->send(message);
		}
		catch (const std::exception& e)
		{
			cout << e.what() << endl;
		}
	}
}


