#include "DataBase.h"

DataBase::DataBase()
{
	zErrMsg = 0;
	s = "Error";
 	int rc = sqlite3_open(DATABASE, &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
	}
}

DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserExist(string username)
{
	bool val = false;
	string command = "SELECT COUNT(*) from t_users where username = '" + username + "';";
	int rc = sqlite3_exec(db, command.c_str(), callbackCount, (void*)&val, &s);
	if (rc != SQLITE_OK)
		return false;
	return val;
} 

bool DataBase::addNewUser(string username, string password, string email)
{
	string command = "INSERT into t_users(username, password, email) values(\"" + username + "\",\"" + password + "\",\"" + email + "\");";
	int rc = sqlite3_exec(db, command.c_str(), NULL, 0, &s);
	if (rc != SQLITE_OK)
		return false;
	else
		return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	bool val = false;
	string command = "SELECT COUNT(*) from t_users where username = '" + username + "' and  password = '" + password + "';";
	int rc = sqlite3_exec(db, command.c_str(), callbackCount, (void*)&val, &s);
	if (rc != SQLITE_OK)
		return false;
	return val;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	vector<Question*> _questions;
	for (int i = 0; i < questionsNo; i++)
	{
		string command = "SELECT * FROM t_questions ORDER BY RANDOM() LIMIT 1;";
		int rc = sqlite3_exec(db, command.c_str(), callbackQuestions, &_questions, &s);
	}
	return _questions;
}

vector<string> DataBase::getBestScores()
{
	vector<string> highScores;

	string command = " SELECT username, SUM(is_correct) from t_players_answers GROUP BY username Order by SUM(is_correct) DESC LIMIT 3;";
	int rc = sqlite3_exec(db, command.c_str(), callbackBestScores, (void*)&highScores, &s);
	if (rc != SQLITE_OK)
		throw exception("ERROR");

	return highScores;
}

vector<string> DataBase::getPersonalStatus(string username)
{
	vector<string> values;
	int counter;
	float avg;

	/* Number Of Games */

	string command = "SELECT COUNT(DISTINCT game_id) from t_players_answers where username = '" + username + "';";
	int rc = sqlite3_exec(db, command.c_str(), callbackPersonalStatus, (void*)&counter, &s);
	if (rc != SQLITE_OK)
		throw exception("ERROR");
	if (counter == 0)
	{
		values.push_back(Helper::getPaddedNumber(0, OTHERS_LENGTH));
		values.push_back(Helper::getPaddedNumber(0, ANSWERS_LENGTH));
		values.push_back(Helper::getPaddedNumber(0, ANSWERS_LENGTH));
		values.push_back(Helper::getPaddedNumber(0, OTHERS_LENGTH));
		return values;
	}
	else
		values.push_back(to_string(counter));

	/* Number Of Right Answers */

	command = "SELECT COUNT(game_id) from t_players_answers where is_correct = 1 and username = '" + username + "';";
	rc = sqlite3_exec(db, command.c_str(), callbackPersonalStatus, (void*)&counter, &s);
	if (rc != SQLITE_OK)
		throw exception("ERROR");
	values.push_back(to_string(counter));

	/* Number Of Wrong Answers */

	command = "SELECT COUNT(game_id) from t_players_answers where is_correct = 0 and username = '" + username + "';";
	rc = sqlite3_exec(db, command.c_str(), callbackPersonalStatus, (void*)&counter, &s);
	if (rc != SQLITE_OK)
		throw exception("ERROR");
	values.push_back(to_string(counter));

	/* Average Answer Time */

	command = "SELECT AVG(answer_time) from t_players_answers where username = '" + username + "';";
	rc = sqlite3_exec(db, command.c_str(), callbackPersonalStatusAvg, (void*)&avg, &s);
	if (rc != SQLITE_OK)
		throw exception("ERROR");
	values.push_back(to_string(float(int(avg*100))/100.00)); // avergae calculation 

	return values;
}

int DataBase::insertNewGame()
{
	int val = 0;
	string command = "INSERT into t_games (status,start_time) values(0,datetime('now'));";
	int rc = sqlite3_exec(db, command.c_str(), NULL, 0, &s);
	if (rc != SQLITE_OK)
		return -1;
	else
	{
		command = "SELECT last_insert_rowid();";
		rc = sqlite3_exec(db, command.c_str(), callbackReturnId, (void*)&val, &s);
		if (rc != SQLITE_OK)
			return false;
		return val;

	}
}

bool DataBase::updateGameStatus(int gameId)
{
	string command = "UPDATE t_games set status = 1, end_time = datetime('now') where game_id = " + to_string(gameId) + ';';
	int rc = sqlite3_exec(db, command.c_str(), NULL, 0, &s);
	if (rc != SQLITE_OK)
		return false;
	else
		return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	string command = "INSERT into t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time)values(" + to_string(gameId) + "," + username + "," + to_string(questionId) + "," + answer + "," + to_string(isCorrect) + "," + to_string(answerTime) + ");";
	int rc = sqlite3_exec(db, command.c_str(), NULL, 0, &s);
	if (rc != SQLITE_OK)
		return false;
	else
		return true;
}

int DataBase::callbackCount(void* val, int argc, char ** argv, char ** azCol)
{
	if (strcmp(argv[0], "0") == 0)
		*((bool*)val) = false; // 0 rows
	else
		*((bool*)val) =  true;
	return 0;
}

int DataBase::callbackQuestions(void* vec, int argc, char ** argv, char ** azCol)
{
	// we've got a random row , now lets make objects out of it
	Question* q = new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]); // New object 
	((vector<Question*>*)vec)->push_back(q); // Insertion
	return 0;
	
}

int DataBase::callbackBestScores(void * vec, int argc, char ** argv, char ** azCol)
{
	((vector<string>*)vec)->push_back(to_string((strlen(argv[0]))));
	((vector<string>*)vec)->push_back(argv[0]);
	((vector<string>*)vec)->push_back(argv[1]);

	return 0;
}

int DataBase::callbackPersonalStatus(void * counter, int argc, char ** argv, char ** azCol)
{
	*((int*)counter) = atoi(argv[0]);
	return 0;
}

int DataBase::callbackPersonalStatusAvg(void * avg, int argc, char ** argv, char ** azCol)
{
	*((float*)avg) = atof(argv[0]);
	return 0;
}


int DataBase::callbackReturnId(void * id, int argc, char ** argv, char ** azCol)
{
	*((int*)id) = atoi(argv[0]);
	return 0;
}
