#include "Room.h"
#include "User.h"


Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_users.push_back(admin);
	_id = id;
	_name = name;
	_admin = admin;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionsTime = questionTime;
}

bool Room::joinRoom(User * user)
{
	bool ret = false;
	if (_users.size() != _maxUsers)
	{
		ret = true;
		_users.push_back(user);
		user->send(to_string(MC_JOIN_ROOM_RESPONSE_SUCCESS) + Helper::getPaddedNumber(_questionNo, QUESTION_LENGTH) + Helper::getPaddedNumber(_questionsTime, QUESTION_LENGTH));
		sendMessage(getUsersListMessage());
	}
	else
	{
		user->send(to_string(MC_JOIN_ROOM_RESPONSE_ROOM_IS_FULL) + Helper::getPaddedNumber(_questionNo, QUESTION_LENGTH) + Helper::getPaddedNumber(_questionsTime, QUESTION_LENGTH));
	}
	return ret;
}

void Room::leaveRoom(User * user)
{
	for (vector<User*>::iterator i = _users.begin(); i != _users.end(); i++)
	{
		if ((*i)->getUsername() == user->getUsername())
		{
			_users.erase(i);
			user->send(to_string(MC_LEAVE_ROOM_RESPONSE_SUCCESS));
			sendMessage(user, getUsersListMessage());
		}
	}
}

int Room::closeRoom(User * user)
{
	if (user->getUsername() == _admin->getUsername())
	{
		sendMessage(to_string(MC_CLOSE_ROOM_RESPONSE));
		for each (User* u in _users)
		{
			if (u->getUsername() != _admin->getUsername())
			{
				u->clearRoom();
			}
		}
		return _id;
	}
	return ROOM_NOT_CLOSED;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	string message = to_string(MC_USERS_IN_ROOM_RESPONSE);
	message += Helper::getPaddedNumber( _users.size(), 1);
	for each (User* u in _users)
	{
		message += Helper::getPaddedNumber(u->getUsername().size(), 2);
		message += u->getUsername();
	}
	return message;
}

int Room::getQuestionsNo()
{
	return _questionNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

void Room::sendMessage(User * user, string message)
{
	for each (User* u in _users)
	{
		if (user == NULL || u->getUsername() != user->getUsername())
		{
			u->send(message);
		}
	}
}

void Room::sendMessage(string message)
{
	sendMessage(NULL, message);
}



