#pragma once

#include "Room.h"
#include "Game.h"
#include "Helper.h"
#include "MessageCodes.h"
#include <WinSock2.h>
#include <string>

using namespace std;


class User
{
public:
	User(string username, SOCKET sock);
	void send(string message); 
	
	// Get & Set 
	string getUsername();
	SOCKET getSocket(); 
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);

	void clearRoom();
	bool createRoom(int roomID, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};

